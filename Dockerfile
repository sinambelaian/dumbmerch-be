# Stage 1: Build the application
FROM golang:1.16 AS builder

WORKDIR /app

# Copy only the necessary files for dependency resolution
COPY go.mod .
COPY go.sum .

# Download and install dependencies
RUN go mod download

# Copy the rest of the application code
COPY . .

# Build the Go application
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o main .

# Stage 2: Create a minimal runtime image
FROM golang:1.16

WORKDIR /app

# Copy only the necessary files from the builder stage
COPY --from=builder /app/main .

# Copy the .env file
COPY .env .

# Expose port 5000 (assuming your Go application uses this port)
EXPOSE 5000

# Run the Go application
CMD ["./main"]
